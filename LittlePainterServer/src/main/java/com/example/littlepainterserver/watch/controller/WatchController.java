package com.example.littlepainterserver.watch.controller;

import com.example.littlepainterserver.entity.Image;
import com.example.littlepainterserver.entity.ImageColor;
import com.example.littlepainterserver.watch.service.WatchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/watch")
public class WatchController {
    @Autowired
    private WatchService watchService;
    //查找用户的画
    @GetMapping("/findPaintByUserName/{username}")
    public List<Image> findPaintByUserName(@PathVariable String username){
        List<Image> imageList=watchService.findPaintByUserName(username);
        imageList.forEach(f->{
            System.out.println("controller"+f);
        });
        return imageList;
    }
    //查找填色图片
    @GetMapping("/findByUserName/{username}")
    public List<ImageColor> findByUserName(@PathVariable String username){
        List<ImageColor> imageList=watchService.findByUserName(username);
        imageList.forEach(f->{
            System.out.println("controller"+f);
        });
        return imageList;
    }

    //根据id删除用户的画，把isDel修改为Y
    @GetMapping("/deleteImageById/{id}")
    public void deleteImageById(@PathVariable int id){
        watchService.deleteImageById(id);
    }
    //根据id删除用户的填色，把isDel修改为Y
    @GetMapping("/deleteIamgeColorId/{id}")
    public void deleteIamgeColorId(@PathVariable int id){
        watchService.deleteIamgeColorId(id);
    }
}
