package com.example.littlepainterserver.watch.service;

import com.example.littlepainterserver.entity.Image;
import com.example.littlepainterserver.entity.ImageColor;

import java.util.List;

public interface WatchService {
    List<Image> findPaintByUserName(String username);
    List<ImageColor> findByUserName(String name);

    void deleteImageById(int id);
    void deleteIamgeColorId(int id);
}
