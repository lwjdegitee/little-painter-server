package com.example.littlepainterserver.watch.repository;

import com.example.littlepainterserver.entity.Image;
import com.example.littlepainterserver.entity.Material;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface WatchRepository extends JpaRepository<Image,String>, JpaSpecificationExecutor {

    List<Image> findAllByUserNameAndIsDel(String username, String isDel);

}
