package com.example.littlepainterserver.watch.service;

import com.example.littlepainterserver.entity.Image;
import com.example.littlepainterserver.entity.ImageColor;
import com.example.littlepainterserver.watch.repository.WatchColorRepository;
import com.example.littlepainterserver.watch.repository.WatchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class WatchServiceImp implements WatchService {
    @Autowired
    private WatchRepository watchRepository;
    @Autowired
    private WatchColorRepository watchColorRepository;
    @Override
    public List<Image> findPaintByUserName(String username) {
        List<Image> images=watchRepository.findAllByUserNameAndIsDel(username,"N");
        return images;
    }
    @Override
    public List<ImageColor> findByUserName(String username){
        List<ImageColor> imageColors=watchColorRepository.findAllColorByUserNameAndIsDel(username,"N");
        return imageColors;
    }

    @Override
    public void deleteImageById(int id) {
        Optional<Image> optional=watchRepository.findById(String.valueOf(id));
        Image image=optional.get();
        image.setIsDel("Y");
        watchRepository.save(image);
    }
    @Override
    public void deleteIamgeColorId(int id) {
        Optional<ImageColor> optional=watchColorRepository.findById(String.valueOf(id));
        ImageColor imageColor=optional.get();
        imageColor.setIsDel("Y");
        watchColorRepository.save(imageColor);
    }
}
