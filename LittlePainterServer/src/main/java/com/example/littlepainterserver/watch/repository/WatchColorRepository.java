package com.example.littlepainterserver.watch.repository;

import com.example.littlepainterserver.entity.ImageColor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface WatchColorRepository extends JpaRepository<ImageColor,String>, JpaSpecificationExecutor {
    List<ImageColor> findAllColorByUserNameAndIsDel(String username, String isDel);

}
