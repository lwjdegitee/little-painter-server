package com.example.littlepainterserver.study.repository;


import com.example.littlepainterserver.entity.MaterialVideo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface StudyVideoRepository extends JpaRepository<MaterialVideo,String>, JpaSpecificationExecutor {
    List<MaterialVideo> findMaterialVideosByName(String name);
}
