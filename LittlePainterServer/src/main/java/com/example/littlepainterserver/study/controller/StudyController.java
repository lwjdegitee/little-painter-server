package com.example.littlepainterserver.study.controller;

import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.entity.MaterialVideo;
import com.example.littlepainterserver.study.service.StudyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/study")
public class StudyController {
    @Autowired
    private StudyService studyService;

    @GetMapping("/findImageByClassify/{classify}")
    public List<Material> findMaterialByName(@PathVariable String classify,
                                             @RequestParam String username){
        List<Material> materials=studyService.findImageByClassify(classify,username);
        return materials;
    }
//    @GetMapping("/findImageByClassify/Commodity")
//    public List<Material> findUnlockByName(@PathVariable String classify){
//        List<Material> materials=studyService.findImageByClassify(classify);
//        return materials;
//    }

    @GetMapping("/findVideoByName/{name}")
    public List<MaterialVideo> findVideoByName(@PathVariable String name){
        List<MaterialVideo> materialVideos=studyService.findVideoByName(name);
        return materialVideos;
    }
}
