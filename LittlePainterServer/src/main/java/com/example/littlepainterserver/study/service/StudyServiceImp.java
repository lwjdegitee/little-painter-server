package com.example.littlepainterserver.study.service;

import com.example.littlepainterserver.entity.Commodity;
import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.entity.MaterialVideo;
import com.example.littlepainterserver.set.repository.UserRepository;
import com.example.littlepainterserver.shoppingmall.entity.MaterialVip;
import com.example.littlepainterserver.shoppingmall.repository.MaterialVipRepository;
import com.example.littlepainterserver.study.repository.StudyRepository;
import com.example.littlepainterserver.study.repository.StudyVideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class StudyServiceImp implements StudyService{
    @Autowired
    private StudyRepository studyRepository;

    @Autowired
    private StudyVideoRepository studyVideoRepository;

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private MaterialVipRepository materialVipRepository;
    @Override
    public List<Material> findImageByClassify(String classify,String username) {
//        List<Material> materials=studyRepository.findMaterialsByClassify(classify);
        List<Material> materials = new ArrayList<>();//必须先实例化

//        System.out.println(classify);
        if (classify.equals("Commodity")){//使用equals判等方法
            System.out.println("This is new unlock!");
            Set<Commodity> commoditySet =userRepository.findUserByName(username).getCommoditySet();
            for (Commodity c: commoditySet) {
//                System.out.println("eName："+c.getEname());
                List<MaterialVip> materials1=materialVipRepository.findMaterialVipsByName(c.getEname());
//                System.out.println(materials1);
//               String id, String name, String chinese, String path, String classify, String isDel) {
                Material material=null;
                for (int i=0;i<materials1.size();i++){
                    material=new Material(materials1.get(i).getId(),
                            materials1.get(i).getName(),
                            materials1.get(i).getChinese(),
                            materials1.get(i).getPath(),
                            materials1.get(i).getClassify(),
                            materials1.get(i).getIsDel());
                    System.out.println("material:"+material);
                    if (material!=null)  {
                        materials.add(i,material);
                        System.out.println("集合"+materials);
                    }
                }
            }
        }else {
            materials=studyRepository.findMaterialsByClassify(classify);
        }
        System.out.println(materials);
        return materials;
    }

    @Override
    public List<MaterialVideo> findVideoByName(String name) {
        List<MaterialVideo> materialVideos=studyVideoRepository.findMaterialVideosByName(name);
        return materialVideos;
    }
}
