package com.example.littlepainterserver.study.repository;

import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.entity.MaterialVideo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

public interface StudyRepository extends JpaRepository<Material,String>, JpaSpecificationExecutor {
    List<Material> findMaterialsByClassify(String classify);

}
