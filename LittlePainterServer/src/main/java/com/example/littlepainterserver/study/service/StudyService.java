package com.example.littlepainterserver.study.service;

import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.entity.MaterialVideo;

import java.util.List;

public interface StudyService {
    List<Material> findImageByClassify(String classify,String username);

    List<MaterialVideo> findVideoByName(String name);
}
