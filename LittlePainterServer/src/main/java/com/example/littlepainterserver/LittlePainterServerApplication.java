package com.example.littlepainterserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LittlePainterServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(LittlePainterServerApplication.class, args);
    }

}
