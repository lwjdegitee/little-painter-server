package com.example.littlepainterserver.palette.service;

import com.example.littlepainterserver.entity.Image;
import com.example.littlepainterserver.entity.MaterialImg;
import org.springframework.stereotype.Service;

import java.util.List;

public interface PaletteService {
    List<MaterialImg> findMaterialImgByName(String name);

    void saveImage(Image image);
}
