package com.example.littlepainterserver.palette.repository;

import com.example.littlepainterserver.entity.Image;
import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.entity.MaterialImg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.List;

public interface PaletteRepository extends JpaRepository<MaterialImg,String>, JpaSpecificationExecutor {
    List<MaterialImg> findMaterialImgByName(String name);


}
