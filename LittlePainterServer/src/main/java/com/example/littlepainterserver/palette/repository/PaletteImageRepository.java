package com.example.littlepainterserver.palette.repository;

import com.example.littlepainterserver.entity.Image;
import com.example.littlepainterserver.entity.MaterialImg;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface PaletteImageRepository extends JpaRepository<Image,String>, JpaSpecificationExecutor {

}
