package com.example.littlepainterserver.palette.service;

import com.example.littlepainterserver.entity.Image;
import com.example.littlepainterserver.entity.MaterialImg;
import com.example.littlepainterserver.palette.repository.PaletteImageRepository;
import com.example.littlepainterserver.palette.repository.PaletteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class PaletteServiceImp implements PaletteService {

    @Autowired
    private PaletteRepository paletteRepository;

    @Autowired
    private PaletteImageRepository  paletteImageRepository;
    @Override
    public List<MaterialImg> findMaterialImgByName(String name) {
        List<MaterialImg> materialImgList=paletteRepository.findMaterialImgByName(name);
        return materialImgList;
    }

    @Override
    public void saveImage(Image image) {
        paletteImageRepository.save(image);
    }
}
