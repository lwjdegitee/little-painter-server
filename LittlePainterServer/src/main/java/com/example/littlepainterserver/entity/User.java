package com.example.littlepainterserver.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.ToString;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@ToString
public class User implements Serializable {
    @Id
    private String id;
    private String name;
    private String password;
    private Integer gold;
    private String isDel;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "t_user_commodity",
            joinColumns = @JoinColumn(name = "name",referencedColumnName ="name" ),
            inverseJoinColumns = @JoinColumn(name = "commodity",referencedColumnName="id"))
    private Set<Commodity> commoditySet=new HashSet<>();

    public Set<Commodity> getCommoditySet() {
        return commoditySet;
    }

    public void setCommoditySet(Set<Commodity> commoditySet) {
        this.commoditySet = commoditySet;
    }

    public User() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getGold() {
        return gold;
    }

    public void setGold(Integer gold) {
        this.gold = gold;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }
}
