package com.example.littlepainterserver.entity;

import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@ToString
public class Commodity implements Serializable {
    //    id
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    // 商品名称
    private String name;
    //商品价格
    private int price;
    //    商品详细描述
    private String details;
    //    图片
    private String img;
    //video
//    private String vido;
    //puzzle
//    private String puzzle;
    //分类
//    private int type;
    //英文命名
    private  String Ename;

    public String getEname() {
        return Ename;
    }

    public void setEname(String ename) {
        Ename = ename;
    }


    @ManyToMany(mappedBy = "commoditySet")
    private Set<User> users = new HashSet<>();


    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }

    public void setId(Integer id) {
        this.id = id;
    }

//    public String getVido() {
//        return vido;
//    }

//    public void setVido(String vido) {
//        this.vido = vido;
//    }

//    public String getPuzzle() {
//        return puzzle;
//    }

//    public void setPuzzle(String puzzle) {
//        this.puzzle = puzzle;
//    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
