package com.example.littlepainterserver.entity;

import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@ToString
public class Material {
    @Id
    private String id;
    private String name;
    private String chinese;
    private String path;
    private String classify;
    private String isDel;

    public Material(){}

    public Material(String id, String name, String chinese, String path, String classify, String isDel) {
        this.id = id;
        this.name = name;
        this.chinese = chinese;
        this.path = path;
        this.classify = classify;
        this.isDel = isDel;
    }

    public String getChinese() {
        return chinese;
    }

    public void setChinese(String chinese) {
        this.chinese = chinese;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getClassify() {
        return classify;
    }

    public void setClassify(String classify) {
        this.classify = classify;
    }

    public String getIsDel() {
        return isDel;
    }

    public void setIsDel(String isDel) {
        this.isDel = isDel;
    }
}
