package com.example.littlepainterserver.color.controller;

import com.example.littlepainterserver.color.service.ColorService;
import com.example.littlepainterserver.entity.ImageColor;
import com.example.littlepainterserver.util.Constant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;


@RestController
@RequestMapping("/color")
public class ColorController {
    @Autowired
    @Lazy
    private ColorService colorService;
    @PostMapping(value = "/uploadImg")
    public void imgUpload(@RequestParam("file") MultipartFile file, HttpServletResponse response) {
        System.out.println("执行图片上传！");
        //获取原始文件名称(包含格式)
        String originalFileName = file.getOriginalFilename();
        originalFileName = originalFileName.replaceAll(",|&|=", "");
        System.out.println("原始文件名称：" + originalFileName);
        String[] form = originalFileName.split("_");

        //设置服务器上图片保存地址
        String path = "D:/PainterClient/img";
        File filePath = new File(path);
        System.out.println("文件的保存路径：" + path);
        if (!filePath.exists() && !filePath.isDirectory()) {
            System.out.println("目录不存在，创建目录:" + filePath);
            filePath.mkdir();
        }

        //获取文件类型，以最后一个`.`为标识
        String type = originalFileName.substring(originalFileName.lastIndexOf(".") + 1);
        System.out.println("文件类型：" + type);
        //在指定路径下创建一个文件
        File targetFile = new File(path, originalFileName);
        //将文件保存到服务器指定位置
        try {
            file.transferTo(targetFile);
            System.out.println("上传成功");
            //将文件在服务器的存储路径返回
            response.setContentType("text/html;charset=utf-8");
            response.getWriter().print(originalFileName);
            //将图片路径名称等信息保存到数据库，方便查找
            ImageColor imageColor = new ImageColor();
            imageColor.setName(originalFileName);
            imageColor.setUserName(form[0]);
            imageColor.setPath(Constant.BASE_URL + originalFileName);
            imageColor.setIsDel("N");
            colorService.saveImage(imageColor);

        } catch (IOException e) {
            System.out.println("上传失败");
            e.printStackTrace();
        }

    }

}
