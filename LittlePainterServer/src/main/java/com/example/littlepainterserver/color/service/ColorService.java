package com.example.littlepainterserver.color.service;


import com.example.littlepainterserver.entity.ImageColor;
import org.springframework.stereotype.Service;
public interface ColorService {
    void saveImage(ImageColor imageColor);
}
