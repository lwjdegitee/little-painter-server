package com.example.littlepainterserver.color.service;

import com.example.littlepainterserver.color.repository.ColorImageRepository;
import com.example.littlepainterserver.entity.ImageColor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ColorServiceImp implements ColorService {
    @Autowired
    private ColorImageRepository colorImageRepository;

    @Override
    public void saveImage(ImageColor imageColor) {
        colorImageRepository.save(imageColor);

    }
}
