package com.example.littlepainterserver.color.repository;

import com.example.littlepainterserver.entity.ImageColor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface ColorImageRepository extends JpaRepository<ImageColor, String>, JpaSpecificationExecutor {
}
