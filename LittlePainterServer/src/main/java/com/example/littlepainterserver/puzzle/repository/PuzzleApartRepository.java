package com.example.littlepainterserver.puzzle.repository;

import com.example.littlepainterserver.puzzle.entity.PuzzleApart;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PuzzleApartRepository extends JpaRepository<PuzzleApart,Integer> {

    List<PuzzleApart> findPuzzleApartsByName(String name);
}
