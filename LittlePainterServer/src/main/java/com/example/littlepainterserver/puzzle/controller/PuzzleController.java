package com.example.littlepainterserver.puzzle.controller;

import com.example.littlepainterserver.puzzle.entity.PuzzleApart;
import com.example.littlepainterserver.puzzle.entity.PuzzleWhole;
import com.example.littlepainterserver.puzzle.service.PuzzleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/puzzle")
public class PuzzleController {
    @Autowired
    private PuzzleService puzzleService;

    @GetMapping("/findPuzzleWholeByName/{name}")
    public PuzzleWhole findPuzzleWholeByName(@PathVariable String name){
        PuzzleWhole puzzleWhole = puzzleService.findPuzzleWholeByName(name);
        return puzzleWhole;
    }

    @GetMapping("/findPuzzleApartsByName/{name}")
    public List<PuzzleApart> findPuzzleApartsByName(@PathVariable String name){
        List<PuzzleApart> puzzleAparts = puzzleService.findPuzzleApartsByName(name);
        return puzzleAparts;
    }
}
