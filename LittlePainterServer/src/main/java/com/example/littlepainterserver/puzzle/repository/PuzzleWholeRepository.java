package com.example.littlepainterserver.puzzle.repository;

import com.example.littlepainterserver.puzzle.entity.PuzzleWhole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PuzzleWholeRepository extends JpaRepository<PuzzleWhole,Integer> {
    public PuzzleWhole findPuzzleWholeByName(String name);
}
