package com.example.littlepainterserver.puzzle.service;

import com.example.littlepainterserver.puzzle.entity.PuzzleApart;
import com.example.littlepainterserver.puzzle.entity.PuzzleWhole;
import com.example.littlepainterserver.puzzle.repository.PuzzleApartRepository;
import com.example.littlepainterserver.puzzle.repository.PuzzleWholeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PuzzleService {
    @Autowired
    private PuzzleWholeRepository puzzleWholeRepository;
    @Autowired
    private PuzzleApartRepository puzzleApartRepository;

    public PuzzleWhole findPuzzleWholeByName(String name){
        PuzzleWhole puzzleWhole = puzzleWholeRepository.findPuzzleWholeByName(name);
        return puzzleWhole;
    }

    public List<PuzzleApart> findPuzzleApartsByName(String name){
        List<PuzzleApart> puzzleAparts = puzzleApartRepository.findPuzzleApartsByName(name);
        return puzzleAparts;
    }

}
