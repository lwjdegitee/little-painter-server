package com.example.littlepainterserver.shoppingmall.repository;

import com.example.littlepainterserver.entity.Commodity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommodityRepository extends JpaRepository<Commodity,Integer>  {


}
