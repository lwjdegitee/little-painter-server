package com.example.littlepainterserver.shoppingmall.service;

import com.example.littlepainterserver.entity.Commodity;
import com.example.littlepainterserver.entity.User;
import com.example.littlepainterserver.entity.UserCommodity;
import com.example.littlepainterserver.set.repository.UserRepository;
import com.example.littlepainterserver.set.service.UserService;
import com.example.littlepainterserver.shoppingmall.repository.CommodityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class CommodityService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private CommodityRepository commodityRepository;

    public List<Commodity> findAll() {
        return commodityRepository.findAll();
    }

    public void IsBuy(int id, String name){
//        userCommodityRepository.save(userCommodity);
        User user= userRepository.findUserByName(name);

        Set<Commodity> commoditySet = new HashSet<>();
        commoditySet= userRepository.findUserByName(name).getCommoditySet();

        Commodity commodity = commodityRepository.findById(id).get();
        commoditySet.add(commodity);
//        commoditySet.add((Commodity) commodityRepository.findAll());

        user.setCommoditySet(commoditySet);

//        commodityRepository.save(commodity);
        userRepository.save(user);

//        System.out.println("存入"+user.getName()+commodity.getId());
    }

}
