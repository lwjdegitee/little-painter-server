package com.example.littlepainterserver.shoppingmall.controller;

import com.example.littlepainterserver.entity.Commodity;
import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.shoppingmall.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/commodity")
public class CommodityController {
    @Autowired
    private CommodityService commodityService;

    @GetMapping("/findAll")
    public List<Commodity> findAll(){
        List<Commodity> commodities=commodityService.findAll();
        System.out.println(commodities.toString());
        return commodities;
    }
}
