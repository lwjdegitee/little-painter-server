package com.example.littlepainterserver.shoppingmall.repository;

import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.shoppingmall.entity.MaterialVip;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MaterialVipRepository extends JpaRepository<MaterialVip,Integer> {
    List<MaterialVip> findMaterialVipsByName(String name);
}
