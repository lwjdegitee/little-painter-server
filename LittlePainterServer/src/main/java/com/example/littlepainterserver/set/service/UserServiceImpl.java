package com.example.littlepainterserver.set.service;

import com.example.littlepainterserver.entity.User;
import com.example.littlepainterserver.set.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.UUID;

@Service
@Slf4j
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;
    @Override
    public User findUserByName(String name) {
        User user = userRepository.findUserByName(name);
        return user;
    }

    @Override
    public User updatePassword( User user) {
        User noUpdate = findUserByName(user.getName());
        noUpdate.setPassword(user.getPassword());
        User update = userRepository.save(noUpdate);
        return update;
    }

    @Override
    public User createUser(User user) {
        user.setId(UUID.randomUUID().toString());
        user.setGold(0);
        user.setIsDel("N");
        User create = userRepository.save(user);
        return create;
    }

    @Override
    public User deleteUser(String name) {
        User user = findUserByName(name);
        user.setIsDel("Y");
        User delete = userRepository.save(user);
        return delete;
    }

    @Override
    public User findGoldByName(String name) {
        User user=userRepository.findUserByName(name);
        return user;
    }

    @Override
    public void addGold(String name) {
        User user=userRepository.findUserByName(name);
        user.setGold(user.getGold()+1);
        userRepository.save(user);
    }
    @Override
    public int getGold(String name,int gold){
        User user = userRepository.findUserByName(name);
        int g;
        if (user.getGold()>=gold){
            user.setGold(user.getGold()-gold);
            userRepository.save(user);
            g=user.getGold();
        }else {
            g= -1;
        }
        return g;
    }

//    @Override
//    public User updatePassword(String name, String oldPassword, String newPassword) {
//        User user = findUserByName(name);
//        String password = user.getPassword();
//        if (password.equals(oldPassword)){
//            user.setPassword(newPassword);
//        }
//        User update = userRepository.save(user);
//        return update;
//    }
//
//    @Override
//    public User createUser(String name, String password) {
//        User user = new User();
//        user.setId(UUID.randomUUID().toString());
//        user.setName(name);
//        user.setPassword(password);
//        user.setGold(0);
//        user.setIsDel("N");
//        User create = userRepository.save(user);
//        return create;
//    }
}
