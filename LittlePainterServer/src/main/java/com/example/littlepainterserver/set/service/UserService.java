package com.example.littlepainterserver.set.service;

import com.example.littlepainterserver.entity.User;

public interface UserService {
    User findUserByName(String name);

    User updatePassword(User user);

    User createUser(User user);

    User deleteUser(String name);

    User findGoldByName(String name);

    void addGold(String name);

    int getGold(String name,int gold);

//    User updatePassword(String name, String oldPassword, String newPassword);
//
//    User createUser(String name, String password);
}
