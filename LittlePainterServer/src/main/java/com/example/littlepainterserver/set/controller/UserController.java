package com.example.littlepainterserver.set.controller;

import com.example.littlepainterserver.entity.User;
import com.example.littlepainterserver.set.exception.BizException;
import com.example.littlepainterserver.set.service.UserService;
import com.example.littlepainterserver.shoppingmall.service.CommodityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/user")
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private CommodityService commodityService;
    @GetMapping("/findUserByName/{name}")
    public User findUserByName(@PathVariable String name){
        User user = userService.findUserByName(name);
        return user;
    }
    @PostMapping("/login")
    public User login(@Valid User user){
        User find = findUserByName(user.getName());
        String isDel = find.getIsDel();
        if ("N".equals(isDel)){
            if (find.getPassword().equals(user.getPassword())){
                return find;
            }else {
                System.out.printf(""+find.getPassword());
                System.out.printf(""+find.getName());
                throw new BizException("密码错误");
            }
        }else {
            throw new BizException("该用户已注销");
        }
    }

    @PostMapping("/createUser")
    public User createUser(@Valid User user){
        User find = findUserByName(user.getName());
        if (find != null){
            throw new BizException("注册失败");
        }else {
            User create = userService.createUser(user);
            return create;
        }
    }

    @PostMapping("/updateUserPassword")
    public User updateUserPassword(@Valid User user){
        User update = userService.updatePassword(user);
        return update;
    }

    @GetMapping("/deleteUser/{name}")
    public User deleteUser(@PathVariable String name){
        User delete = userService.deleteUser(name);
        return delete;
    }

    @GetMapping("/findGoldByName/{name}")
    public User findGoldByName(@PathVariable String name){
        User user=userService.findGoldByName(name);
        return user;
    }

    @GetMapping("/addGold/{name}")
    public void addGold(@PathVariable String name){
        userService.addGold(name);

    }

    @GetMapping("/changeGoldByName/{name}")
    public int findGoldByName( @PathVariable String name,
                                    @RequestParam int p,
                               @RequestParam String id){
        System.out.println(id);
        System.out.println(p);
        int a =userService.getGold(name,p);

        if (a!= -1){
            commodityService.IsBuy(Integer.parseInt(id),name);

        }
//        User user=userService.findUserByName(name);
//        int gold=user.getGold()-p;
//        System.out.printf("成功查询到积分");
//        return gold;
        System.out.print(a);
        return a;
    }

}
