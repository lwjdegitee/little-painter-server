package com.example.littlepainterserver.set.exception;

import com.example.littlepainterserver.set.domain.Result;
import com.example.littlepainterserver.set.utils.ResultUtil;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;



@RestControllerAdvice
public class BizExceptionHandler {
    @ExceptionHandler(BizException.class)
    public Result handlerException(Exception e){
        return ResultUtil.error("500",e.getMessage(),null);
    }
}
