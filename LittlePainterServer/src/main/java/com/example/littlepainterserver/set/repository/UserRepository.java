package com.example.littlepainterserver.set.repository;

import com.example.littlepainterserver.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface UserRepository extends JpaRepository<User,String>, JpaSpecificationExecutor {
    User findUserByName(String name);


}
