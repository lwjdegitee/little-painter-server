package com.example.littlepainterserver.set.domain;

import lombok.Data;

@Data
public class Result<T>{
    private String code;
    private String msg;
    private T data;

}
