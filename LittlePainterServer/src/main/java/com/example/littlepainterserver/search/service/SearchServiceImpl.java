package com.example.littlepainterserver.search.service;

import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.search.repository.SearchRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SearchServiceImpl implements SearchService{
    @Autowired
    private SearchRepository searchRepository;

    @Override
    public Material findMaterialByChinese(String chinese) {
        Material material = searchRepository.findMaterialByChinese(chinese);
        System.out.println(material);
        return material;
    }
}
