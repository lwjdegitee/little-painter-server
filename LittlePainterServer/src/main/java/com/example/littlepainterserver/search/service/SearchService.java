package com.example.littlepainterserver.search.service;

import com.example.littlepainterserver.entity.Material;
import org.springframework.stereotype.Service;

public interface SearchService {
    Material findMaterialByChinese(String chinese);
}
