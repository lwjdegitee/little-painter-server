package com.example.littlepainterserver.search.controller;

import com.example.littlepainterserver.entity.Material;
import com.example.littlepainterserver.search.service.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/search")
public class SearchController {
    @Autowired
    private SearchService searchService;

    @GetMapping("findMaterialByChinese/{chinese}")
    public Material findMaterialByCName(@PathVariable String chinese){
        Material material = searchService.findMaterialByChinese(chinese);
        return material;
    }
}
