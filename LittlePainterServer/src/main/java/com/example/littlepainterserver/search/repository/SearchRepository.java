package com.example.littlepainterserver.search.repository;

import com.example.littlepainterserver.entity.Material;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SearchRepository extends JpaRepository<Material,String> {
    Material findMaterialByChinese(String chinese);
}
